//
//  tuplet.swift
//
//
//  Created by Ruben Zilibowitz on 9/01/2016.
//
//

import UIKit

extension Vex {
    public class Tuplet {
        public enum Location {
            case Top
            case Bottom
        }
        
        public struct Options {
            var num_notes : Int?
            var beats_occupied : Int?
        }
        
        // options
        var num_notes : Int
        var beats_occupied : Int
        
        var notes : [StemmableNote]
        var bracketed : Bool!
        var ratioed : Bool
        var point : CGFloat
        var y_pos : CGFloat
        var x_pos : CGFloat
        var width : CGFloat
        var location : Location
        
        
        public init(notes : [StemmableNote], options : Options) throws {
            
            self.notes = notes
            if let num_notes = options.num_notes {
                self.num_notes = num_notes
            }
            else {
                self.num_notes = notes.count
            }
            if let beats_occupied = options.beats_occupied {
                self.beats_occupied = beats_occupied
            }
            else {
                self.beats_occupied = 2
            }
            self.ratioed = false
            self.point = 28
            self.y_pos = 16
            self.x_pos = 100
            self.width = 200
            self.location = .Top
            
            if (notes.count == 0) {
                throw Vex.RuntimeError(code: "BadArguments", message: "No notes provided for tuplet.")
            }
            
            if (notes.count == 1) {
                throw Vex.RuntimeError(code: "BadArguments", message: "Too few notes for tuplet.")
            }
            
            self.bracketed = (notes[0].beam == nil)
            
            Vex.formatter.AlignRestsToNotes(notes, align_all_notes: true, align_tuplets: true)
            self.resolveGlyphs()
            self.attach()
        }
        
        public func resolveGlyphs() {
            
        }
        
        public func attach() {
            
        }
    }
}
