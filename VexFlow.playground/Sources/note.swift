//
//  note.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

// ## Description
//
// This file implements an abstract interface for notes and chords that
// are rendered on a stave. Notes have some common properties: All of them
// have a value (e.g., pitch, fret, etc.) and a duration (quarter, half, etc.)
//
// Some notes have stems, heads, dots, etc. Most notational elements that
// surround a note are called *modifiers*, and every note has an associated
// array of them. All notes also have a rendering context and belong to a stave.



import UIKit

extension Vex {
    public class Note : Tickable, VexCategory {
        // To create a new note you need to provide a `note_struct`, which consists
        // of the following fields:
        //
        // `type`: The note type (e.g., `r` for rest, `s` for slash notes, etc.)
        // `dots`: The number of dots, which affects the duration.
        // `duration`: The time length (e.g., `q` for quarter, `h` for half, `8` for eighth etc.)
        //
        // The range of values for these parameters are available in `src/tables.js`.
        public struct NoteStruct {
            var type : String? = "r"
            var dots : Int? = 0
            var duration : String = "q"
            var align_center : Bool?
            var duration_override : String?
        }
        
        public struct RenderOptions {
            var annotation_spacing : CGFloat = 5
            var stave_padding : CGFloat = 12
        }
        
        var duration : String
        var dots : Int
        var noteType : String
        
        var glyph : DurationCode.Data?
        
        var x : CGFloat = 0
        var isrest : Bool = false
        
        // Note to play for audio players.
        var playNote : Int?
        
        var ys : [CGFloat] = [] // list of y coordinates for each note
        // we need to hold on to these for ties and beams.
        
        var render_options : RenderOptions = RenderOptions()
        
        override var stave : Stave? {
            didSet {
                if let stave = stave {
                    self.ys = [stave.getYForLine(0)] // Update Y values if the stave is changed.
                    self.context = stave.context
                }
            }
        }
        
        var lineNumber : Int = 0       // Get the stave line number for the note.
        var lineForRest : Int = 0      // Get the stave line number for rest.
        var isDotted : Bool { get { return dots > 0 } }
        var hasStem : Bool = false
        
        // Positioning variables
        var width : CGFloat = 0             // Width in pixels calculated after preFormat
        var extraLeftPx : CGFloat = 0       // Extra room on left for offset note head
        var extraRightPx : CGFloat = 0      // Extra room on right for offset note head
        var left_modPx : CGFloat = 0        // Max width of left modifiers
        var right_modPx : CGFloat = 0       // Max width of right modifiers
        
        public init(noteStruct : NoteStruct) {
            // Parse `note_struct` and get note properties.
            guard let initData = Vex.parseNoteData(noteStruct.duration, dots: noteStruct.dots, type: noteStruct.type) else { fatalError("Could not parse note data") }
            
            self.duration = initData["duration"] as! String
            self.dots = initData["dots"] as! Int
            self.noteType = initData["type"] as! String
            
            // Get the glyph code for this note from the font.
            self.glyph = Vex.durationToGlyph(self.duration, type: self.noteType)
            super.init()
            
            if let duration = noteStruct.duration_override {
                // Custom duration
                self.duration = duration
            }
            else {
                // Default duration
                let ticks = initData["ticks"] as! Int
                self.intrinsicTicks = Rational(ticks,1)!
            }
            
            if let align_center = noteStruct.align_center {
                self.align_center = align_center
            }
        }
        
        public func addModifier(modifier : Modifier, index : Int? = nil) {
            modifier.note = self
            modifier.index = (index ?? 0)
            self.modifiers.append(modifier)
            self.preFormatted = false
        }
        
        var modifierStartXY : CGPoint {
            get {
                return CGPointMake(self.absoluteX, self.ys[0])
            }
        }
        
        override var voice : Voice? {
            didSet {
                preFormatted = false
            }
        }
        
        override var tickContext : TickContext? {
            didSet {
                preFormatted = false
            }
        }
        
        override var preFormatted : Bool {
            didSet {
                if preFormatted {
                    // Maintain the width of left and right modifiers in pixels.
                    if let extra = self.tickContext?.extraPx {
                        self.left_modPx = max(self.left_modPx, extra.left);
                        self.right_modPx = max(self.right_modPx, extra.right);
                    }
                }
            }
        }
        
        // Get the absolute `X` position of this note's tick context. This
        // excludes x_shift, so you'll need to factor it in if you're
        // looking for the post-formatted x-position.
        var absoluteX : CGFloat {
            get {
                guard let tickContext = self.tickContext else { fatalError("Note needs a TickContext assigned for an X-Value") }
                
                // Position note to left edge of tick context.
                var x = tickContext.x
                if let stave = self.stave {
                    x += stave.getNoteStartX() + self.render_options.stave_padding
                }
                
                if (self.align_center) {
                    x += self.center_x_shift
                }
                
                return x
            }
        }
        
        // Get bounds and metrics for this note.
        //
        // Returns a struct with fields:
        // `width`: The total width of the note (including modifiers.)
        // `noteWidth`: The width of the note head only.
        // `left_shift`: The horizontal displacement of the note.
        // `modLeftPx`: Start `X` for left modifiers.
        // `modRightPx`: Start `X` for right modifiers.
        // `extraLeftPx`: Extra space on left of note.
        // `extraRightPx`: Extra space on right of note.
        override var metrics : Metrics? {
            get {
                if !self.preFormatted {
                    fatalError("Can't call getMetrics on an unformatted note.")
                }
                var modLeftPx : CGFloat = 0
                var modRightPx : CGFloat = 0
                if let modifierContext = self.modifierContext {
                    modLeftPx = modifierContext.state.left_shift
                    modRightPx = modifierContext.state.right_shift
                }
                return Metrics(
                    width: self.width,
                    noteWidth: width -
                        modLeftPx - modRightPx -
                        self.extraLeftPx - self.extraRightPx,
                    left_shift: self.x_shift, // TODO(0xfe): Make style consistent,
                    
                    // Modifiers, accidentals etc.
                    modLeftPx: modLeftPx,
                    modRightPx: modRightPx,
                    
                    // Displaced note head on left or right.
                    extraLeftPx: self.extraLeftPx,
                    extraRightPx: self.extraRightPx
                )
            }
        }
        
        func getWidth() -> CGFloat {
            return self.width + (self.modifierContext?.width ?? 0)
        }
        
        func getX() -> CGFloat {
            guard let tickContext = self.tickContext else { fatalError("Note needs a TickContext assigned for an X-Value") }
            return self.x_shift + tickContext.x
        }
        
        public override func draw() {
            if let stave = self.stave {
                self.draw(stave, x_shift: self.x_shift)
            }
        }
        
        func draw(stave : Vex.Stave, x_shift : CGFloat) {
        }
        static func format(notes : [VexCategory], state : Vex.ModifierContext.State, context : Vex.ModifierContext) {
        }
        static func postFormat(notes : [VexCategory], context : Vex.ModifierContext) {
        }
        
        public func getLineForRest() -> CGFloat {
            return 0
        }
    }
}
