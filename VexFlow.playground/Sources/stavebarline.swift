//
//  stavebarline.swift
//  
//
//  Created by Ruben Zilibowitz on 2/01/2016.
//
//

import UIKit

extension Vex {
    public class Barline : StaveModifier {
        public enum BarlineType {
            case Single
            case Double
            case End
            case Repeat_Begin
            case Repeat_End
            case Repeat_Both
            case None
        }
        
        var type : BarlineType
        var thickness : CGFloat
        
        public init(type : BarlineType, x : CGFloat) {
            self.thickness = Vex.STAVE_LINE_THICKNESS
            self.type = type
            super.init(x: x)
        }
        
        override func draw(stave : Vex.Stave, x_shift : CGFloat) {
            switch self.type {
            case .Single:
                self.drawVerticalBar(stave, x: self.x, double_bar: false)
                break
            case .Double:
                self.drawVerticalBar(stave, x: self.x, double_bar: true)
                break
            case .End:
                self.drawVerticalEndBar(stave,  x: self.x)
                break
            case .Repeat_Begin:
                // If the barline is shifted over (in front of clef/time/key)
                // Draw vertical bar at the beginning.
                if x_shift > 0 {
                    self.drawVerticalBar(stave, x: self.x)
                }
                self.drawRepeatBar(stave, x: self.x + x_shift, begin: true)
                break
            case .Repeat_End:
                self.drawRepeatBar(stave, x: self.x, begin: false)
                break
            case .Repeat_Both:
                self.drawRepeatBar(stave, x: self.x, begin: false)
                self.drawRepeatBar(stave, x: self.x, begin: true)
                break
            case .None:
                // nothing to draw
                break
            }
        }
        
        func drawVerticalBar(stave : Vex.Stave, x : CGFloat, double_bar : Bool = false) {
            guard let context = stave.context else { return }
            let topY = stave.getYForLine(0)
            let botY = stave.getYForLine(CGFloat(stave.num_lines - 1)) + self.thickness
            if double_bar {
                CGContextFillRect(context, CGRectMake(x - 3, topY, 1, botY - topY))
            }
            CGContextFillRect(context, CGRectMake(x, topY, 1, botY - topY))
        }
        
        func drawVerticalEndBar(stave : Vex.Stave, x : CGFloat) {
            guard let context = stave.context else { return }
            let topY = stave.getYForLine(0)
            let botY = stave.getYForLine(CGFloat(stave.num_lines - 1)) + self.thickness
            CGContextFillRect(context, CGRectMake(x - 5, topY, 1, botY - topY))
            CGContextFillRect(context, CGRectMake(x - 2, topY, 3, botY - topY))
        }
        
        func drawRepeatBar(stave : Vex.Stave, x : CGFloat, begin : Bool) {
            guard let context = stave.context else { return }
            
            let topY = stave.getYForLine(0)
            let botY = stave.getYForLine(CGFloat(stave.num_lines - 1)) + self.thickness
            var x_shift : CGFloat = 3
            
            if (!begin) {
                x_shift = -5
            }
            
            CGContextFillRect(context, CGRectMake(x + x_shift, topY, 1, botY - topY))
            CGContextFillRect(context, CGRectMake(x - 2, topY, 3, botY - topY))
            
            let dot_radius : CGFloat = 2
            
            // Shift dots left or right
            if (begin) {
                x_shift += 4
            } else {
                x_shift -= 4
            }
            
            let dot_x = (x + x_shift) + (dot_radius / 2)
            
            // calculate the y offset based on number of stave lines
            var y_offset = CGFloat(stave.num_lines - 1) * stave.spacing_between_lines_px
            y_offset = (y_offset / 2) - (stave.spacing_between_lines_px / 2)
            var dot_y = topY + y_offset + (dot_radius / 2)
            
            // draw the top repeat dot
            CGContextFillEllipseInRect(context, CGRectMake(dot_x - dot_radius*0.5, dot_y - dot_radius*0.5, dot_radius, dot_radius))
            
            //draw the bottom repeat dot
            dot_y += stave.spacing_between_lines_px
            CGContextFillEllipseInRect(context, CGRectMake(dot_x - dot_radius*0.5, dot_y - dot_radius*0.5, dot_radius, dot_radius))
        }
    }
}
