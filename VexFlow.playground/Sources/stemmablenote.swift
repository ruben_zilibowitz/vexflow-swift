//
//  stemmablenote.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

import UIKit

extension Vex {
    public class StemmableNote : Note {
        var stem : Stem?
        var beam : Beam?
        var stem_direction : Stem.Direction?
        var keyProps : [KeyProperties]?
    }
}
