//
//  modifiercontext.swift
//
//
//  Created by Ruben Zilibowitz on 9/01/2016.
//
//

import UIKit

extension Vex {
    public class ModifierContext {
        public struct State {
            var left_shift : CGFloat = 0
            var right_shift : CGFloat = 0
            var text_line : CGFloat = 0
            var top_text_line : CGFloat = 0
        }
        
        // Current modifiers
        var modifiers : [String : [VexCategory]] = [:]
        
        // Formatting data.
        var preFormatted : Bool = false
        var postFormatted : Bool = false
        var formatted : Bool = false
        var width : CGFloat = 0
        var spacing : CGFloat = 0
        var state : State = State()
        
        var preformat : [VexCategory.Type] =
           [Vex.Note.self]
        
        var postformat : [VexCategory.Type] = [Vex.Note.self]
        
        public func addModifier(modifier : Modifier) {
            let type = Vex.category(modifier)
            if (self.modifiers[type] == nil) {
                self.modifiers[type] = []
            }
            self.modifiers[type]!.append(modifier)
            modifier.modifier_context = self
            self.preFormatted = false
        }
        
        public struct Metrics {
            var width : CGFloat
            var spacing : CGFloat
            var extra_left_px : CGFloat
            var extra_right_px : CGFloat
        }
        
        var metrics : Metrics {
            get {
                if (!self.formatted) {
                    fatalError("UnformattedModifier: Unformatted modifier has no metrics.")
                }
                return Metrics(width: self.state.left_shift + self.state.right_shift + self.spacing, spacing: self.spacing, extra_left_px: self.state.left_shift, extra_right_px: self.state.right_shift)
            }
        }
        
        public func preFormat() {
            if (!self.preFormatted) {
                self.preformat.forEach({ modifier in
                    let category = Vex.typeCategory(modifier)
                    guard let mods = self.modifiers[category] else { return }
                    modifier.format(mods, state: self.state, context: self)
                })
                // Update width of this modifier context
                self.width = self.state.left_shift + self.state.right_shift
                self.preFormatted = true
            }
        }
        
        public func postFormat() {
            if (!self.postFormatted) {
                self.postformat.forEach({ modifier in
                    let category = Vex.typeCategory(modifier)
                    guard let mods = self.modifiers[category] else { return }
                    modifier.postFormat(mods, context: self)
                })
            }
        }
    }
}
