//
//  modifier.swift
//
//
//  Created by Ruben Zilibowitz on 7/01/2016.
//
//

import UIKit

extension Vex {
    public class Modifier : VexCategory {
        public enum Position {
            case Left
            case Right
            case Above
            case Below
        }
        
        var x : CGFloat = 0
        var width : CGFloat = 0
        
        // Modifiers are attached to a note and an index. An index is a
        // specific head in a chord.
        var note : Note?
        var index : Int?
        
        // The `text_line` is reserved space above or below a stave.
        var text_line : CGFloat = 0
        var position : Position = .Left
        var modifier_context : ModifierContext?
        var x_shift : CGFloat = 0
        var y_shift : CGFloat = 0
        var spacingFromNextModifier : CGFloat = 0
        
        public init(position : Position, x : CGFloat, width : CGFloat) {
            self.position = position
            self.x = x
            self.width = width
        }
        
        public func set_x_shift(x : CGFloat) {
            self.x_shift = 0
            if (self.position == .Left) {
                self.x_shift -= x
            } else {
                self.x_shift += x
            }
        }
        
        public func draw(stave: Vex.Stave, x_shift: CGFloat) {
            fatalError("draw() not implemented for this modifier.")
        }
        
        static func format(blahs : [VexCategory], state : Vex.ModifierContext.State, context : Vex.ModifierContext) {
            fatalError("format() not implemented for this modifier.")
        }
        
        static func postFormat(blahs : [VexCategory], context : Vex.ModifierContext) {
            fatalError("postFormat() not implemented for this stave modifier.")
        }
    }
}
