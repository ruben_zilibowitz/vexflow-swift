//
//  beam.swift
//  VexFlowTest
//
//  Created by Ruben Zilibowitz on 19/01/2016.
//  Copyright © 2016 Zilibowitz Productions. All rights reserved.
//

// ## Description
//
// This file implements `Beams` that span over a set of `StemmableNotes`.
//


import UIKit

extension Vex {
    public class Beam {
        var ticks : Rational
        var stem_direction : Stem.Direction
        var postFormatted : Bool
        var notes : [StemmableNote]
        var beam_count : Int
        var break_on_indices : [Int]
        
        struct RenderOptions {
            var beam_width:CGFloat = 5
            var max_slope:CGFloat = 0.25
            var min_slope:CGFloat = -0.25
            var slope_iterations:CGFloat = 20
            var slope_cost:CGFloat = 100
            var show_stemlets:Bool = false
            var stemlet_extension:Int = 7
            var partial_beam_length:CGFloat = 10
            var flat_beams:Bool = false
            var min_flat_beam_offset:CGFloat = 15
        }
        
        var render_options : RenderOptions
        
        init(notes : [StemmableNote], auto_stem : Bool) {
            if notes.count <= 1 {
                fatalError("Too few notes for beam.")
            }
            
            // Validate beam line, direction and ticks.
            self.ticks = notes[0].intrinsicTicks
            
            if let quarterNote = Vex.durationToTicks("4") where self.ticks >= Rational(quarterNote,1)! {
                fatalError("Beams can only be applied to notes shorter than a quarter note.")
            }
            
            self.stem_direction = .Up
            
            for note in notes {
                if (note.hasStem && note.stem_direction != nil) {
                    self.stem_direction = note.stem_direction!
                    break
                }
            }
            
            self.postFormatted = false
            self.notes = notes
            self.break_on_indices = []
            self.render_options = RenderOptions()
            self.beam_count = 0
            
            var stem_direction = self.stem_direction
            // Figure out optimal stem direction based on given notes
            if (auto_stem && notes[0].dynamicType == StaveNote.self)  {
                stem_direction = self.calculateStemDirection(notes)
            }
            else if (auto_stem && notes[0].dynamicType == TabNote.self) {
                // Auto Stem TabNotes
                let stem_weight = notes.reduce(0, combine: {memo, note in return memo + (note.stem_direction?.rawValue ?? 0)})
                stem_direction = stem_weight > -1 ? .Up : .Down
            }
            
            // Apply stem directions and attach beam to notes
            for note in notes {
                if (auto_stem) {
                    note.stem_direction = stem_direction
                    self.stem_direction = stem_direction
                }
                note.beam = self
            }
            
            self.beam_count = self.getBeamCount()
        }
        
        // Get the max number of beams in the set of notes
        func getBeamCount() -> Int {
            let beamCounts = self.notes.map({ note in
                return note.glyph?.beam_count ?? 0
            })
            
            let maxBeamCount = beamCounts.reduce(0, combine: { max, beamCount in
                return beamCount > max ? beamCount : max
            })
            
            return maxBeamCount
        }
        
        func calculateStemDirection(notes : [StemmableNote]) -> Stem.Direction {
            var lineSum = 0
            notes.forEach({ note in
                if (note.keyProps != nil) {
                    note.keyProps!.forEach({ keyProp in
                        lineSum += ((keyProp.line ?? 0) - 3)
                    })
                }
            })
            
            if (lineSum >= 0) {
                return .Down
            }
            else {
                return .Up
            }
        }
    }
}
