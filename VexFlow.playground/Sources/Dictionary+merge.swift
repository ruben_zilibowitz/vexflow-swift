//
//  Dictionary+merge.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

import Foundation

extension Dictionary {
    mutating func unionInPlace(
        dictionary: Dictionary<Key, Value>) {
            for (key, value) in dictionary {
                self[key] = value
            }
    }
}
