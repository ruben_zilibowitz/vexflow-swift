//
//  voice.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

import UIKit

extension Vex {
    public class Voice {
        
        // Modes allow the addition of ticks in three different ways:
        //
        // STRICT: This is the default. Ticks must fill the voice.
        // SOFT:   Ticks can be added without restrictions.
        // FULL:   Ticks do not need to fill the voice, but can't exceed the maximum
        //         tick length.
        public enum Mode {
            case Strict
            case Soft
            case Full
        }
        
        public struct Time {
            var num_beats : Int = 4
            var beat_value : Int = 4
            var resolution : Int = Vex.Resolution
        }
        
        var time : Time
        var totalTicks : Rational
        var resolutionMultiplier : Int = 1
        var tickables : [Tickable] = []
        var ticksUsed : Rational = Rational(0,1)!
        var smallestTickCount : Rational
        var largestTickWidth : CGFloat = 0
        var stave : Stave? {
            didSet {
                self.boundingBox = nil  // Reset bounding box so we can reformat
            }
        }
        
        private var boundingBox_ : CGRect?
        var boundingBox : CGRect? {
            get {
                if let bb = boundingBox_ {
                    return bb
                }
                else {
                    if (self.stave == nil) {
                        fatalError("NoStave: Can't get bounding box without stave.")
                    }
                    let stave = self.stave
                    var boundingBox : CGRect?
                    
                    for tickable in self.tickables {
                        tickable.stave = stave
                        
                        if let bb = tickable.boundingBox {
                            boundingBox = boundingBox == nil ? bb : CGRectUnion(boundingBox!, bb)
                        }
                    }
                    
                    boundingBox_ = boundingBox
                    
                    return boundingBox_
                }
            }
            set {
                self.boundingBox_ = newValue
            }
        }
        
        // Do we care about strictly timed notes
        var mode : Mode = .Strict
        
        // This must belong to a VoiceGroup
        var voiceGroup : VoiceGroup?
        
        var actualResolution : Int { get { return self.resolutionMultiplier * self.time.resolution } }
        
        var preFormatted : Bool = false
        
        public func isComplete() -> Bool {
            if self.mode == .Strict || self.mode == .Full {
                return self.ticksUsed == self.totalTicks
            }
            else {
                return true
            }
        }
        
        public func setStrict(strict : Bool) {
            self.mode = strict ? .Strict : .Soft
        }
        
        public func addTickable(tickable : Tickable) {
            if !tickable.ignore_ticks {
                let ticks = tickable.ticks
                
                self.ticksUsed = self.ticksUsed + ticks
                
                if (self.mode == .Strict || self.mode == .Full) && self.ticksUsed > self.totalTicks {
                    self.totalTicks = self.totalTicks - ticks
                }
                
                // Track the smallest tickable for formatting.
                if ticks < self.smallestTickCount {
                    self.smallestTickCount = ticks
                }
                
                self.resolutionMultiplier = self.ticksUsed.denominator
                
                // Expand total ticks using denominator from ticks used.
                // nb: this line does not do anything in Swift
                // probably should comment it out
                self.totalTicks = self.totalTicks + Rational(0, self.ticksUsed.denominator)!
            }
            
            // Add the tickable to the line.
            self.tickables.append(tickable)
            tickable.voice = self
        }
        
        // Add an array of tickables to the voice.
        public func addTickables(tickables : [Tickable]) {
            for tickable in tickables {
                self.addTickable(tickable)
            }
        }
        
        // Preformats the voice by applying the voice's stave to each note.
        public func preFormat() {
            if (!self.preFormatted) {
                self.tickables.forEach({tickable in
                    if tickable.stave == nil {
                        tickable.stave = self.stave
                    }
                })
                self.preFormatted = true
            }
        }
        
        // Render the voice onto the canvas `context` and an optional `stave`.
        // If `stave` is omitted, it is expected that the notes have staves
        // already set.
        public func draw(context : CGContextRef, stave : Stave? = nil) {
            var boundingBox : CGRect?
            
            var i : Int = 0
            for tickable in self.tickables {
                
                // Set the stave if provided
                if (stave != nil) {
                    tickable.stave = stave
                }
                
                if i == 0 {
                    boundingBox = tickable.boundingBox
                }
                
                if i > 0 && boundingBox != nil {
                    if let tickable_bb = tickable.boundingBox {
                        boundingBox = CGRectUnion(boundingBox!, tickable_bb)
                    }
                }
                
                tickable.context = context
                tickable.draw()
                
                i++
            }
            
            self.boundingBox = boundingBox
        }
        
        init() {
            let time = Time()
            self.time = time
            self.totalTicks = Rational(time.num_beats * time.resolution, time.beat_value)!
            self.smallestTickCount = self.totalTicks
        }
    }
}
