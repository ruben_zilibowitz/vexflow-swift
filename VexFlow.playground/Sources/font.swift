//
//  font.swift
//
//
//  Created by Ruben Zilibowitz on 3/01/2016.
//
//

import UIKit

extension Vex {
    public class Font {
        public init(glyphs: [String : Font.Glyph],
            cssFontWeight : String,
            ascender : CGFloat,
            underlinePosition : CGFloat,
            cssFontStyle : String,
            boundingBox : CGRect,
            resolution : CGFloat,
            descender : CGFloat,
            familyName : String,
            lineHeight : CGFloat,
            underlineThickness : CGFloat) {
            
            self.glyphs = glyphs
            self.cssFontWeight = cssFontWeight
            self.ascender = ascender
            self.underlinePosition = underlinePosition
            self.cssFontStyle = cssFontStyle
            self.boundingBox = boundingBox
            self.resolution = resolution
            self.descender = descender
            self.familyName = familyName
            self.lineHeight = lineHeight
            self.underlineThickness = underlineThickness
        }
        
        public class Glyph {
            public struct Command {
                public enum Action {
                    case move
                    case line
                    case quadCurve
                    case bezierCurve
                }
                var action : Action
                var values : Array<CGFloat>
            }
            var x_min : CGFloat
            var x_max : CGFloat
            var ha : CGFloat
            var outline : [Command]
            
            public func renderOutline(ctx : CGContextRef, scale : CGFloat, x_pos : CGFloat, y_pos : CGFloat) {
                CGContextBeginPath(ctx)
                
                CGContextMoveToPoint(ctx, x_pos, y_pos)
                
                for cmd in self.outline {
                    switch cmd.action {
                    case .move:
                        CGContextMoveToPoint(ctx, x_pos + cmd.values[0] * scale, y_pos + cmd.values[1] * -scale)
                        break
                    case .line:
                        CGContextAddLineToPoint(ctx, x_pos + cmd.values[0] * scale, y_pos + cmd.values[1] * -scale)
                        break
                    case .quadCurve:
                        CGContextAddQuadCurveToPoint(ctx, x_pos + cmd.values[2] * scale, y_pos + cmd.values[3] * -scale, x_pos + cmd.values[0] * scale, y_pos + cmd.values[1] * -scale)
                        break
                    case .bezierCurve:
                        CGContextAddCurveToPoint(ctx, x_pos + cmd.values[2] * scale, y_pos + cmd.values[3] * -scale, x_pos + cmd.values[4] * scale, y_pos + cmd.values[5] * -scale, x_pos + cmd.values[0] * scale, y_pos + cmd.values[1] * -scale)
                        break
                    }
                }
                
                CGContextFillPath(ctx)
            }
            public init(x_min: CGFloat, x_max: CGFloat, ha: CGFloat, outline: [Command]) {
                self.x_min = x_min
                self.x_max = x_max
                self.ha = ha
                self.outline = outline
            }
        }
        
        var glyphs : [String : Font.Glyph]
        var cssFontWeight : String
        var ascender : CGFloat
        var underlinePosition : CGFloat
        var cssFontStyle : String
        var boundingBox : CGRect
        var resolution : CGFloat
        var descender : CGFloat
        var familyName : String
        var lineHeight : CGFloat
        var underlineThickness : CGFloat
        
        public func renderGlyph(ctx : CGContextRef, x_pos : CGFloat, y_pos : CGFloat, point : CGFloat, val : String) throws {
            guard let glyph = self.glyphs[val] else {
                throw Vex.RuntimeError(code: "BadGlyph", message: "Glyph \(val) is not initialized.")
            }
            let scale = point * 72.0 / (self.resolution * 100.0)
            glyph.renderOutline(ctx, scale: scale, x_pos: x_pos, y_pos: y_pos)
        }
        
        public static func load(name : String) -> Font? {
            if let fontPath = NSBundle.mainBundle().pathForResource(name, ofType: "json") {
                if let fontData = NSFileManager.defaultManager().contentsAtPath(fontPath) {
                    do {
                        if let json = try NSJSONSerialization.JSONObjectWithData(fontData, options: []) as? [String : AnyObject] {
                            guard let glyphs = json["glyphs"] as? [String : AnyObject] else { return nil }
                            guard let cssFontWeight = json["cssFontWeight"] as? String else { return nil }
                            guard let ascender = json["ascender"] as? CGFloat else { return nil }
                            guard let underlinePosition = json["underlinePosition"] as? CGFloat else { return nil }
                            guard let cssFontStyle = json["cssFontStyle"] as? String else { return nil }
                            guard let boundingBox = json["boundingBox"] as? [String : CGFloat] else { return nil }
                            guard let resolution = json["resolution"] as? CGFloat else { return nil }
                            guard let descender = json["descender"] as? CGFloat else { return nil }
                            guard let familyName = json["familyName"] as? String else { return nil }
                            guard let lineHeight = json["lineHeight"] as? CGFloat else { return nil }
                            guard let underlineThickness = json["underlineThickness"] as? CGFloat else { return nil }
                            
                            guard let boundingBox_x_min = boundingBox["xMin"] else { return nil }
                            guard let boundingBox_y_min = boundingBox["yMin"] else { return nil }
                            guard let boundingBox_x_max = boundingBox["xMax"] else { return nil }
                            guard let boundingBox_y_max = boundingBox["yMax"] else { return nil }
                            
                            let boundingBox_CGRect = CGRectMake(boundingBox_x_min, boundingBox_y_min, boundingBox_x_max - boundingBox_x_min, boundingBox_y_max - boundingBox_y_min)
                            
                            var finalGlyphs : [String : Font.Glyph] = [:]
                            for key in glyphs.keys {
                                guard let glyph = glyphs[key] as? [String : AnyObject] else { return nil }
                                guard let x_min = glyph["x_min"] as? CGFloat else { return nil }
                                guard let x_max = glyph["x_max"] as? CGFloat else { return nil }
                                guard let ha = glyph["ha"] as? CGFloat else { return nil }
                                var commands : [Vex.Font.Glyph.Command] = []
                                if let o = glyph["o"] as? String {
                                    let cmds = o.componentsSeparatedByString(" ")
                                    var idx = 0
                                    while idx < (cmds.count-1) {
                                        if cmds[idx] == "m" {
                                            idx++
                                            let a = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let b = CGFloat((cmds[idx] as NSString).floatValue)
                                            commands.append(Glyph.Command(action: .move, values: [a,b]))
                                        }
                                        else if cmds[idx] == "l" {
                                            idx++
                                            let a = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let b = CGFloat((cmds[idx] as NSString).floatValue)
                                            commands.append(Glyph.Command(action: .line, values: [a,b]))
                                        }
                                        else if cmds[idx] == "q" {
                                            idx++
                                            let a = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let b = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let c = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let d = CGFloat((cmds[idx] as NSString).floatValue)
                                            commands.append(Glyph.Command(action: .quadCurve, values: [a,b,c,d]))
                                        }
                                        else if cmds[idx] == "b" {
                                            idx++
                                            let a = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let b = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let c = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let d = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let e = CGFloat((cmds[idx] as NSString).floatValue)
                                            idx++
                                            let f = CGFloat((cmds[idx] as NSString).floatValue)
                                            commands.append(Glyph.Command(action: .bezierCurve, values: [a,b,c,d,e,f]))
                                        }
                                        else {
                                            NSLog("Unknown glyph outline command: \(cmds[idx])")
                                            return nil
                                        }
                                        idx++
                                    }
                                    
                                    let finalGlyph = Glyph(x_min: x_min, x_max: x_max, ha: ha, outline: commands)
                                    finalGlyphs[key] = finalGlyph
                                }
                            }
                            
                            return Font(glyphs: finalGlyphs, cssFontWeight: cssFontWeight, ascender: ascender, underlinePosition: underlinePosition, cssFontStyle: cssFontStyle, boundingBox: boundingBox_CGRect, resolution: resolution, descender: descender, familyName: familyName, lineHeight: lineHeight, underlineThickness: underlineThickness)
                        }
                    }
                    catch let error as NSError {
                        NSLog("JSON read error \(error)")
                    }
                }
            }
            
            return nil
        }
    }
}
