//
//  stave.swift
//
//
//  Created by Ruben Zilibowitz on 2/01/2016.
//
//

// nb: not finished

import UIKit

extension Vex {
    public class Stave {
        public static let THICKNESS = (Vex.STAVE_LINE_THICKNESS > 1 ? Vex.STAVE_LINE_THICKNESS : 0)
        
        public struct Options {
            public struct LineOptions {
                var visible : Bool
            }
            var vertical_bar_width : CGFloat?
            var glyph_spacing_px : CGFloat?
            var num_lines : Int?
            var fill_style : UIColor?
            var spacing_between_lines_px : CGFloat?
            var space_above_staff_ln : CGFloat?
            var space_below_staff_ln : CGFloat?
            var top_text_position : CGFloat?
            var bottom_text_position : CGFloat?
            var line_config : Array<LineOptions>?
        }
        
        // options
        var vertical_bar_width : CGFloat
        var glyph_spacing_px : CGFloat
        var num_lines : Int {
            didSet {
                self.resetLines()
            }
        }
        var fill_style : UIColor
        var spacing_between_lines_px : CGFloat
        var space_above_staff_ln : CGFloat
        var space_below_staff_ln : CGFloat
        var top_text_position : CGFloat
        var bottom_text_position : CGFloat
        var line_config : Array<Options.LineOptions>
        
        func merge(ops : Options) {
            if let x = ops.vertical_bar_width {
                self.vertical_bar_width = x
            }
            if let x = ops.glyph_spacing_px {
                self.glyph_spacing_px = x
            }
            if let x = ops.num_lines {
                self.num_lines = x
            }
            if let x = ops.fill_style {
                self.fill_style = x
            }
            if let x = ops.spacing_between_lines_px {
                self.spacing_between_lines_px = x
            }
            if let x = ops.space_above_staff_ln {
                self.space_above_staff_ln = x
            }
            if let x = ops.space_above_staff_ln {
                self.space_above_staff_ln = x
            }
            if let x = ops.space_below_staff_ln {
                self.space_below_staff_ln = x
            }
            if let x = ops.top_text_position {
                self.top_text_position = x
            }
            if let x = ops.bottom_text_position {
                self.bottom_text_position = x
            }
            if let x = ops.line_config {
                self.line_config = x
            }
        }
        
        var x : CGFloat {
            didSet {
                let shift : CGFloat = oldValue - self.x
                self.glyph_start_x += shift
                self.glyph_end_x += shift
                self.start_x += shift
                self.end_x += shift
                
                for var modifier in self.modifiers {
                    modifier.x = modifier.x + shift
                }
            }
        }
        var y : CGFloat
        var width : CGFloat {
            didSet {
                self.glyph_end_x = self.x + self.width
                self.end_x = self.glyph_end_x
                // reset the x position of the end barline (TODO(0xfe): This makes no sense)
                // this.modifiers[1].setX(this.end_x);
            }
        }
        var height : CGFloat
        var glyph_start_x : CGFloat
        var glyph_end_x : CGFloat
        var start_x : CGFloat
        var end_x : CGFloat
        public var context : CGContextRef? {
            didSet {
                for glyph in self.glyphs {
                    glyph.context = context
                }
            }
        }
        var glyphs : [Vex.GlyphSpacer]
        var end_glyphs : [Vex.GlyphSpacer]
        var modifiers : [VexCategory]
        var measure : Int
        var clef : ClefType?
        var font : UIFont
        
        var boundingBox : CGRect {
            get {
                return CGRectMake(x, y, width, getBottomY() - y)
            }
        }
        
        public init(x : CGFloat, y : CGFloat, width : CGFloat, options : Options? = nil) {
            self.x = x
            self.y = y
            self.width = width
            self.height = 0
            self.glyph_start_x = x + 5
            self.glyph_end_x = x + width
            self.start_x = self.glyph_start_x
            self.end_x = self.glyph_end_x
            self.measure = 0
            self.glyphs = []
            self.end_glyphs = []
            self.modifiers = []
            self.font = UIFont(name: "GillSans", size: 8)!
            
            // default options
            self.vertical_bar_width = 10
            self.glyph_spacing_px = 10
            self.num_lines = 5
            self.fill_style = UIColor.grayColor()
            self.spacing_between_lines_px = 10
            self.space_above_staff_ln = 4
            self.space_below_staff_ln = 4
            self.top_text_position = 1
            self.bottom_text_position = 6
            self.line_config = Array()
            
            if let ops = options {
                self.merge(ops)
            }
            
            self.resetLines()
            
            self.modifiers.append(Vex.Barline(type: .Single, x: self.x))
            self.modifiers.append(Vex.Barline(type: .Single, x: self.x + self.width))
        }
        
        func resetLines() {
            for (var i = 0; i < self.num_lines; i++) {
                self.line_config.append((Options.LineOptions(visible: true)))
            }
            self.height = (CGFloat(num_lines) + space_above_staff_ln) * spacing_between_lines_px
            self.bottom_text_position = CGFloat(self.num_lines + 1)
        }
        
        // Bar line functions
        func setBegBarType(type : Barline.BarlineType) {
            // Only valid bar types at beginning of stave is none, single or begin repeat
            if (type == .Single || type == .Repeat_Begin || type == .None) {
                    self.modifiers[0] = Vex.Barline(type: type, x: self.x)
            }
        }
        
        func setEndBarType(type : Barline.BarlineType) {
            // Repeat begin not valid at end of stave
            if (type != .Repeat_Begin) {
                self.modifiers[1] = Vex.Barline(type: type, x: self.x + self.width)
            }
        }
        
        /**
        * Gets the pixels to shift from the beginning of the stave
        * following the modifier at the provided index
        * @param  {Number} index The index from which to determine the shift
        * @return {Number}       The amount of pixels shifted
        */
        func getModifierXShift(index : Int) -> CGFloat {
            
//            var x = self.glyph_start_x
            var bar_x_shift : CGFloat = 0
            
            for (var i = 0; i < index + 1 && i < self.glyphs.count; ++i) {
                let glyph = self.glyphs[i]
//                x += glyph.width ?? 0
                bar_x_shift += glyph.width ?? 0
            }
            
            // Add padding after clef, time sig, key sig
            if (bar_x_shift > 0) {
                bar_x_shift += self.vertical_bar_width + 10
            }
            
            return bar_x_shift
        }
        
        // Coda & Segno Symbol functions
        func setRepetitionTypeLeft(type : Vex.Repetition.RepetitionType, y : CGFloat) {
            self.modifiers.append(Vex.Repetition(type: type, x: self.x, y_shift: y))
        }

        
        func setRepetitionTypeRight(type : Vex.Repetition.RepetitionType, y : CGFloat) {
            setRepetitionTypeLeft(type, y: y)
        }
        
        // Volta functions
        func setVoltaType(type : Volta.VoltaType, number_t : Int, y : CGFloat) {
            self.modifiers.append(Volta(volta: type, number: number_t, x: self.x, y_shift: y))
        }
        
        // Section functions
        func setSection(section : Int, y : CGFloat) {
            self.modifiers.append(Section(section: section, x: self.x, y_shift: y))
        }
        
        // Tempo functions
//        func setTempo(tempo, y) {
//            self.modifiers.push(new Vex.Flow.StaveTempo(tempo, self.x, y))
//        }
        
        func setNoteStartX(x : CGFloat) -> Stave {
            self.start_x = x
            return self
        }
        
        func getNoteStartX() -> CGFloat {
            var start_x = self.start_x
            
            // Add additional space if left barline is REPEAT_BEGIN and there are other
            // start modifiers than barlines
            if (self.modifiers.count > 2) {
                if let leftModifier = self.modifiers[0] as? Vex.Barline {
                    if leftModifier.type == .Repeat_Begin {
                        start_x += 20
                    }
                }
            }
            
            return start_x
        }
        
        func getBottomY() -> CGFloat {
            let spacing = self.spacing_between_lines_px
            let score_bottom = self.getYForLine(CGFloat(self.num_lines)) + (CGFloat(self.space_below_staff_ln) * spacing)
            return score_bottom
        }
        
        func getBottomLineY() -> CGFloat {
            return self.getYForLine(CGFloat(self.num_lines))
        }
        
        func getYForLine(line : CGFloat) -> CGFloat {
            let spacing = self.spacing_between_lines_px
            let headroom = CGFloat(self.space_above_staff_ln)
            let y = self.y + ((line * spacing) + (headroom * spacing)) - (Stave.THICKNESS / 2)
            return y
        }
        
        func getYForTopText(line : CGFloat? = nil) -> CGFloat {
            let l = line ?? 0
            return self.getYForLine(-l - self.top_text_position)
        }
        
        func getYForBottomText(line : CGFloat? = nil) -> CGFloat {
            let l = line ?? 0
            return self.getYForLine(self.bottom_text_position + l)
        }
        
        func getYForNote(line : CGFloat) -> CGFloat {
            let spacing = self.spacing_between_lines_px
            let headroom = CGFloat(self.space_above_staff_ln)
            let y = self.y + (headroom * spacing) + (5 * spacing) - (CGFloat(line) * spacing)
            return y
        }
        
        func getYForGlyphs() -> CGFloat {
            return self.getYForLine(3)
        }
        
        func makeSpacer(width : CGFloat) -> Vex.GlyphSpacer {
            return GlyphSpacer(stave: self, width: width)
        }
        
        func addGlyph(glyph : Vex.GlyphSpacer) {
            glyph.stave = self
            self.glyphs.append(glyph)
            self.start_x += (glyph.width ?? 0)
        }
        
        func addEndGlyph(glyph : Vex.GlyphSpacer) {
            glyph.stave = self
            self.end_glyphs.append(glyph)
            self.end_x -= (glyph.width ?? 0)
        }
        
        func addModifier(modifier : Vex.StaveModifier) {
            self.modifiers.append(modifier)
            modifier.addToStave(self, firstGlyph: (self.glyphs.count == 0))
        }
        
        func addEndModifier(modifier : Vex.StaveModifier) {
            self.modifiers.append(modifier)
            modifier.addToStaveEnd(self, firstGlyph: (self.end_glyphs.count == 0))
        }
        
        func addClef(clef : ClefType = .Treble, size : Clef.Size = .Default, annotation : String?) {
            self.clef = clef
            self.addModifier(Clef(clef: clef, size: size, annotation: annotation))
        }
        
        func addTrebleGlyph() {
            self.clef = .Treble
            self.addGlyph(Vex.Glyph(code: "v83", point: 40, stave: self))
        }
        
        // MARK: draw functions
        
        public func draw() {
            guard let context = self.context else { return }
            
            let num_lines = self.num_lines
            let width = self.width
            var x = self.x
            
            CGContextSetStrokeColorWithColor(context, self.fill_style.CGColor)
            CGContextSetFillColorWithColor(context, self.fill_style.CGColor)
            
            // Render lines
            for (var line = 0; line < num_lines; line++) {
                let y = self.getYForLine(CGFloat(line))
                CGContextFillRect(context, CGRectMake(x, y, width, Vex.STAVE_LINE_THICKNESS))
            }
            
            CGContextSetStrokeColorWithColor(context, UIColor.blackColor().CGColor)
            CGContextSetFillColorWithColor(context, UIColor.blackColor().CGColor)
            
            // Render glyphs
            x = self.glyph_start_x
            for glyph in self.glyphs {
                glyph.renderToStave(x)
                x += (glyph.width ?? 0)
            }
            
            // Render end glyphs
            x = self.glyph_end_x
            for glyph in self.end_glyphs {
                x -= (glyph.width ?? 0)
                glyph.renderToStave(x)
            }
            
            // Draw the modifiers (bar lines, coda, segno, repeat brackets, etc.)
            for (var i = 0; i < self.modifiers.count; i++) {
                self.modifiers[i].draw(self, x_shift: self.getModifierXShift(i))
            }
            
            // Render measure numbers
            if (self.measure > 0) {
                guard let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as? NSMutableParagraphStyle else { return }
                textStyle.alignment = NSTextAlignment.Center
                
                let textFontAttributes : [String : AnyObject] = [
                    NSFontAttributeName: self.font,
                    NSForegroundColorAttributeName: self.fill_style,
                    NSParagraphStyleAttributeName: textStyle
                ]
                
                let y = self.getYForTopText(0) + 3
                let text = "\(self.measure)"
                let size = text.sizeWithAttributes(textFontAttributes)
                let textRect: CGRect = CGRectMake(self.x - size.width/2, y, size.width, size.height)
                text.drawInRect(CGRectOffset(textRect, 0, 1), withAttributes: textFontAttributes)
            }
        }
    }
}
