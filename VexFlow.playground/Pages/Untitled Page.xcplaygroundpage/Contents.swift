//: Playground - noun: a place where people can play

import UIKit
import XCPlayground

Vex.roundN(1.4, n: 0.5)
Vex.roundN(0.3, n: 0.5)
Vex.roundN(-1.3, n: 0.5)
Vex.roundN(4, n: 0.3)

Vex.Stave(x: 1, y: 2, width: 3)

// Fonts
Vex.Font.load("gonville_all")
Vex.Font.load("gonville_original")
Vex.Font.load("gonville")
Vex.Font.load("vexflow_font")

// Rationals
let original = Rational(25, 21)!
let intermediate = Double(rational: original)
let result = Rational(intermediate)
Double(rational: original - result)

let view = UIView(frame: CGRectMake(0,0,600,300))
view.backgroundColor = UIColor.whiteColor()

UIGraphicsBeginImageContext(view.frame.size)
let stave = Vex.Stave(x: 10, y: 0, width: 500)
let bar = Vex.Barline(type: .Single, x: 10)
stave.context = UIGraphicsGetCurrentContext()
stave.draw()
let image = UIGraphicsGetImageFromCurrentImageContext()
UIGraphicsEndImageContext()

view.addSubview(UIImageView(image: image))



