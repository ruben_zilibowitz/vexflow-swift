//
//  staverepetition.swift
//
//
//  Created by Ruben Zilibowitz on 7/01/2016.
//
//

import UIKit

extension Vex {
    public class Repetition : StaveModifier {
        public enum RepetitionType {
            case None         // no coda or segno
            case Coda_Left    // coda at beginning of stave
            case Coda_Right   // coda at end of stave
            case Segno_Left   // segno at beginning of stave
            case Segno_Right  // segno at end of stave
            case DC           // D.C. at end of stave
            case DC_Al_Coda   // D.C. al coda at end of stave
            case DC_Al_Fine   // D.C. al Fine end of stave
            case DS           // D.S. at end of stave
            case DS_Al_Coda   // D.S. al coda at end of stave
            case DS_Al_Fine   // D.S. al Fine at end of stave
            case Fine         // Fine at end of stave
        }
        
        var symbol_type : RepetitionType
        var x_shift : CGFloat
        var y_shift : CGFloat
        var font : UIFont
        
        public init(type : RepetitionType, x : CGFloat, y_shift : CGFloat) {
            self.symbol_type = type
            self.x_shift = 0
            self.y_shift = y_shift
            self.font = UIFont(name: "Times Bold Italic", size: 12)!
            super.init(x: x)
        }
        
        public override func draw(stave: Vex.Stave, x_shift: CGFloat) {
            switch self.symbol_type {
            case .None: break
            case .Coda_Right:
                self.drawCodaFixed(stave, x: x + stave.width)
                break
            case .Coda_Left:
                self.drawSymbolText(stave, x: x, text: "Coda", draw_coda: true)
                break
            case .Segno_Left:
                self.drawSignoFixed(stave, x: x)
                break
            case .Segno_Right:
                self.drawSignoFixed(stave, x: x + stave.width)
                break
            case .DC:
                self.drawSymbolText(stave, x: x, text: "D.C.", draw_coda: false)
                break
            case .DC_Al_Coda:
                self.drawSymbolText(stave, x: x, text: "D.C. al", draw_coda: true)
                break
            case .DC_Al_Fine:
                self.drawSymbolText(stave, x: x, text: "D.C. al Fine", draw_coda: false)
                break
            case .DS:
                self.drawSymbolText(stave, x: x, text: "D.S.", draw_coda: false)
                break
            case .DS_Al_Coda:
                self.drawSymbolText(stave, x: x, text: "D.S. al", draw_coda: true)
                break
            case .DS_Al_Fine:
                self.drawSymbolText(stave, x: x, text: "D.S. al Fine", draw_coda: false)
                break
            case .Fine:
                self.drawSymbolText(stave, x: x, text: "Fine", draw_coda: false)
                break
            }
        }
        
        public func drawCodaFixed(stave : Stave, x : CGFloat) {
            guard let context = stave.context else { return }
            let y = stave.getYForTopText(CGFloat(stave.num_lines)) + self.y_shift
            do {
                try Vex.defaultFont.renderGlyph(context, x_pos: self.x + x + self.x_shift, y_pos: y + 25, point: 40, val: "v4d")
            }
            catch let error as Vex.RuntimeError {
                fatalError(error.toString())
            }
            catch {
                fatalError("Unknown error")
            }
        }
        
        public func drawSignoFixed(stave : Stave, x : CGFloat) {
            guard let context = stave.context else { return }
            let y = stave.getYForTopText(CGFloat(stave.num_lines)) + self.y_shift
            do {
                try Vex.defaultFont.renderGlyph(context, x_pos: self.x + x + self.x_shift, y_pos: y + 25, point: 30, val: "v8c")
            }
            catch let error as Vex.RuntimeError {
                fatalError(error.toString())
            }
            catch {
                fatalError("Unknown error")
            }
        }
        
        
        public func drawSymbolText(stave : Stave, x : CGFloat, text : String, draw_coda : Bool) {
            guard let context = stave.context else { return }
            guard let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as? NSMutableParagraphStyle else { return }
            textStyle.alignment = NSTextAlignment.Center
            let textFontAttributes : [String : AnyObject] = [
                NSFontAttributeName: self.font,
                NSForegroundColorAttributeName: UIColor.blackColor(),
                NSParagraphStyleAttributeName: textStyle
            ]
            let textSize = text.sizeWithAttributes(textFontAttributes)
            
            // Default to right symbol
            var text_x = 0 + self.x_shift
            var symbol_x = x + self.x_shift
            if (self.symbol_type == Vex.Repetition.RepetitionType.Coda_Left) {
                // Offset Coda text to right of stave beginning
                text_x = self.x + stave.vertical_bar_width
                symbol_x = text_x + textSize.width + 12
            } else {
                // Offset Signo text to left stave end
                symbol_x = self.x + x + stave.width - 5 + self.x_shift
                text_x = symbol_x - textSize.width - 12
            }
            let y = stave.getYForTopText(CGFloat(stave.num_lines)) + self.y_shift
            if (draw_coda) {
                do {
                    try Vex.defaultFont.renderGlyph(context, x_pos: symbol_x, y_pos: y, point: 40, val: "v4d")
                }
                catch let error as Vex.RuntimeError {
                    fatalError(error.toString())
                }
                catch {
                    fatalError("Uknown error")
                }
            }
            
            let textRect: CGRect = CGRectMake(text_x, y + 5, textSize.width, textSize.height)
            text.drawInRect(textRect, withAttributes: textFontAttributes)
        }
    }
}
