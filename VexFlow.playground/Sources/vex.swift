//
//  vex.swift
//
//
//  Created by Ruben Zilibowitz on 2/01/2016.
//
//

import UIKit

public class Vex {
    public class RuntimeError : ErrorType {
        private var code : String
        private var message : String
        init(code : String, message : String) {
            self.code = code
            self.message = message
        }
        func toString() -> String {
            return ("RuntimeError (\(self.code)): \(self.message)")
        }
    }
    
    
    // Merge `destination` hash with `source` hash, overwriting like keys
    // in `source` if necessary.
    public static func merge<T: Hashable>(var destination : [T : AnyObject], source : [T : AnyObject]) {
        for key in source.keys {
            destination[key] = source[key]
        }
    }
    
    // Round number to nearest fractional value (`.5`, `.25`, etc.)
    public static func roundN(x : CGFloat, n : CGFloat) -> CGFloat {
        let ax = x < 0 ? -x : x
        let sx = (x < 0) ? -CGFloat(1) : CGFloat(1)
        if (ax % n >= n / 2) {
            return sx * (CGFloat(Int(ax / n)) * n + n)
        }
        else {
            return sx * CGFloat(Int(ax / n)) * n
        }
    }
    
    // Locate the mid point between stave lines. Returns a fractional line if a space.
    public static func midLine(a : CGFloat, b : CGFloat) -> CGFloat {
        var mid_line = b + (a - b) / 2
        if (mid_line % 2 > 0) {
            mid_line = Vex.roundN(mid_line * 10, n: 5) / 10
        }
        return mid_line
    }
    
    public static func drawDot(ctx : CGContextRef, x : CGFloat, y : CGFloat, color : UIColor = UIColor.redColor()) {
        CGContextFillEllipseInRect(ctx, CGRectMake(x-0.5, y-0.5, 1, 1))
    }
    
    public static var defaultFont : Vex.Font = Vex.Font.load("vexflow_font")!
    
    public static var formatter : Vex.Formatter = Vex.Formatter()
    
    public static var Resolution : Int = 120
    
    static func category(type : VexCategory) -> String {
        return String(type.dynamicType)
    }
    
    static func typeCategory(type : VexCategory.Type) -> String {
        return String(type)
    }
}
