//
//  stavesection.swift
//
//
//  Created by Ruben Zilibowitz on 7/01/2016.
//
//

import UIKit

extension Vex {
    public class Section : Modifier {
        var section : Int
        var font : UIFont
        
        public init(section : Int, x : CGFloat, y_shift : CGFloat) {
            self.section = section
            self.font = UIFont(name: "Helvetica Bold", size: 12)!
            
            super.init(position: .Above, x: x, width: 16)
            
            self.x_shift = 0
            self.y_shift = y_shift
        }
        
        override public func draw(stave: Vex.Stave, x_shift: CGFloat) {
            guard let context = stave.context else { return }
            
            guard let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as? NSMutableParagraphStyle else { return }
            textStyle.alignment = NSTextAlignment.Center
            let textFontAttributes : [String : AnyObject] = [
                NSFontAttributeName: self.font,
                NSForegroundColorAttributeName: UIColor.blackColor(),
                NSParagraphStyleAttributeName: textStyle
            ]
            
            let text = "\(self.section)"
            let textSize = text.sizeWithAttributes(textFontAttributes)
            
            var width = textSize.width + 6  // add left & right padding
            if width < 18 {
                width = 18
            }
            
            var height = textSize.height
            if height < 20 {
                height = 20
            }
            
            //  Seems to be a good default y
            let y = stave.getYForTopText(3) + self.y_shift
            var x = self.x + self.x_shift
            
            CGContextSetLineWidth(context, 2)
            CGContextStrokeRect(context, CGRectMake(x, y, width, height))
            
            x += (width - textSize.width) / 2
            
            text.drawInRect(CGRectMake(x, y, width, height), withAttributes: textFontAttributes)
        }
    }
}
