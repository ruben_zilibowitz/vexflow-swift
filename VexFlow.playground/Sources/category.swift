//
//  category.swift
//
//
//  Created by Ruben Zilibowitz on 7/01/2016.
//
//

import UIKit

protocol VexCategory {
    var x : CGFloat { get set }
    func draw(stave : Vex.Stave, x_shift : CGFloat)
    static func format(notes : [VexCategory], state : Vex.ModifierContext.State, context : Vex.ModifierContext)
    static func postFormat(notes : [VexCategory], context : Vex.ModifierContext)
}
