//
//  tables.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

import UIKit

protocol VexDurationCodeProtocol {
    var code_head: String?     { get }
    var head_width: CGFloat?     { get }
    var rest: Bool?     { get }
    var position: String?     { get }
    var stem: Bool?     { get }
    var stem_offset: CGFloat?     { get }
    var flag: Bool?     { get }
    var stem_up_extension: CGFloat?     { get }
    var stem_down_extension: CGFloat?     { get }
    var gracenote_stem_up_extension: CGFloat?     { get }
    var gracenote_stem_down_extension: CGFloat?     { get }
    var tabnote_stem_up_extension: CGFloat?     { get }
    var tabnote_stem_down_extension: CGFloat?     { get }
    var dot_shiftY: CGFloat?     { get }
    var line_above: CGFloat?     { get }
    var line_below: CGFloat?     { get }
}

extension Vex {
    public static let STEM_WIDTH : CGFloat = 1.5
    public static let STEM_HEIGHT : CGFloat = 32
    public static let STAVE_LINE_THICKNESS : CGFloat = 2
    public static let RESOLUTION : Int = 16384
    
    public struct ClefProperties {
        var line_shift : CGFloat
    }
    
    public enum ClefType {
        case Treble
        case Bass
        case Tenor
        case Alto
        case Soprano
        case Percussion
        case MezzoSoprano
        case BaritoneC
        case BaritoneF
        case SubBass
        case French
        
        public var properties : ClefProperties {
            get {
                switch self {
                case Treble:
                    return ClefProperties(line_shift: 0)
                case Bass:
                    return ClefProperties(line_shift: 6)
                case Tenor:
                    return ClefProperties(line_shift: 4)
                case Alto:
                    return ClefProperties(line_shift: 3)
                case Soprano:
                    return ClefProperties(line_shift: 1)
                case Percussion:
                    return ClefProperties(line_shift: 0)
                case MezzoSoprano:
                    return ClefProperties(line_shift: 2)
                case BaritoneC:
                    return ClefProperties(line_shift: 5)
                case BaritoneF:
                    return ClefProperties(line_shift: 5)
                case SubBass:
                    return ClefProperties(line_shift: 7)
                case French:
                    return ClefProperties(line_shift: -1)
                }
            }
        }
        
        func toGlyphInfo() -> Vex.Clef.GlyphInfo {
            switch self {
            case .Treble:
                return Vex.Clef.GlyphInfo(code: "v83", line: 3)
            case .Bass:
                return Vex.Clef.GlyphInfo(code: "v79", line: 1)
            case .Alto:
                return Vex.Clef.GlyphInfo(code: "vad", line: 2)
            case .Tenor:
                return Vex.Clef.GlyphInfo(code: "vad", line: 1)
            case .Percussion:
                return Vex.Clef.GlyphInfo(code: "v59", line: 2)
            case .Soprano:
                return Vex.Clef.GlyphInfo(code: "vad", line: 4)
            case .MezzoSoprano:
                return Vex.Clef.GlyphInfo(code: "vad", line: 3)
            case .BaritoneC:
                return Vex.Clef.GlyphInfo(code: "vad", line: 0)
            case .BaritoneF:
                return Vex.Clef.GlyphInfo(code: "v79", line: 2)
            case .SubBass:
                return Vex.Clef.GlyphInfo(code: "v79", line: 0)
            case .French:
                return Vex.Clef.GlyphInfo(code: "v83", line: 4)
            }
        }
        
        func toString() -> String {
            switch self {
            case .Treble:
                return "treble"
            case .Bass:
                return "bass"
            case .Alto:
                return "alto"
            case .Tenor:
                return "tenor"
            case .Percussion:
                return "percussion"
            case .Soprano:
                return "soprano"
            case .MezzoSoprano:
                return "mezzo-soprano"
            case .BaritoneC:
                return "baritone-c"
            case .BaritoneF:
                return "baritone-f"
            case .SubBass:
                return "subbass"
            case .French:
                return "french"
            }
        }
    }
    
    public struct NoteProperties {
        var index : Int
        var int_val : Int?
        var rest : Bool = false
        var accidental : String?
        var octave : Int?
        var code : String?
        var shift_right : CGFloat?
        
        public init(index:Int,int_val:Int? = nil,rest:Bool = false,accidental:String? = nil,octave:Int? = nil,code:String? = nil,shift_right:CGFloat? = nil) {
            self.index = index
            self.int_val = int_val
            self.rest = rest
            self.accidental = accidental
            self.octave = octave
            self.code = code
            self.shift_right = shift_right
        }
    }
    
    public static let NoteValues : [String : NoteProperties] = [
        "C":  NoteProperties(index: 0, int_val: 0),
        "CN": NoteProperties(index: 0, int_val: 0, accidental: "n"),
        "C#": NoteProperties(index: 0, int_val: 1, accidental: "#"),
        "C##": NoteProperties(index: 0, int_val: 2, accidental: "##"),
        "CB": NoteProperties(index: 0, int_val: -1, accidental: "b"),
        "CBB": NoteProperties(index: 0, int_val: -2, accidental: "bb"),
        "D":  NoteProperties(index: 1, int_val: 2),
        "DN": NoteProperties(index: 1, int_val: 2, accidental: "n"),
        "D#": NoteProperties(index: 1, int_val: 3, accidental: "#"),
        "D##": NoteProperties(index: 1, int_val: 4, accidental: "##"),
        "DB": NoteProperties(index: 1, int_val: 1, accidental: "b"),
        "DBB": NoteProperties(index: 1, int_val: 0, accidental: "bb"),
        "E":  NoteProperties(index: 2, int_val: 4),
        "EN": NoteProperties(index: 2, int_val: 4, accidental: "n"),
        "E#": NoteProperties(index: 2, int_val: 5, accidental: "#"),
        "E##": NoteProperties(index: 2, int_val: 6, accidental: "##"),
        "EB": NoteProperties(index: 2, int_val: 3, accidental: "b"),
        "EBB": NoteProperties(index: 2, int_val: 2, accidental: "bb"),
        "F":  NoteProperties(index: 3, int_val: 5),
        "FN": NoteProperties(index: 3, int_val: 5, accidental: "n"),
        "F#": NoteProperties(index: 3, int_val: 6, accidental: "#"),
        "F##": NoteProperties(index: 3, int_val: 7, accidental: "##"),
        "FB": NoteProperties(index: 3, int_val: 4, accidental: "b"),
        "FBB": NoteProperties(index: 3, int_val: 3, accidental: "bb"),
        "G":  NoteProperties(index: 4, int_val: 7),
        "GN": NoteProperties(index: 4, int_val: 7, accidental: "n"),
        "G#": NoteProperties(index: 4, int_val: 8, accidental: "#"),
        "G##": NoteProperties(index: 4, int_val: 9, accidental: "##"),
        "GB": NoteProperties(index: 4, int_val: 6, accidental: "b"),
        "GBB": NoteProperties(index: 4, int_val: 5, accidental: "bb"),
        "A":  NoteProperties(index: 5, int_val: 9),
        "AN": NoteProperties(index: 5, int_val: 9, accidental: "n"),
        "A#": NoteProperties(index: 5, int_val: 10, accidental: "#"),
        "A##": NoteProperties(index: 5, int_val: 11, accidental: "##"),
        "AB": NoteProperties(index: 5, int_val: 8, accidental: "b"),
        "ABB": NoteProperties(index: 5, int_val: 7, accidental: "bb"),
        "B":  NoteProperties(index: 6, int_val: 11),
        "BN": NoteProperties(index: 6, int_val: 11, accidental: "n"),
        "B#": NoteProperties(index: 6, int_val: 12, accidental: "#"),
        "B##": NoteProperties(index: 6, int_val: 13, accidental: "##"),
        "BB": NoteProperties(index: 6, int_val: 10, accidental: "b"),
        "BBB": NoteProperties(index: 6, int_val: 9, accidental: "bb"),
        "R": NoteProperties(index: 6, int_val: 9, rest: true), // Rest
        "X": NoteProperties(index: 6, accidental: "", octave: 4, code: "v3e", shift_right: 5.5)
    ]
    
    public struct KeyProperties {
        var code : String
        var shift_right : CGFloat
        var key: Int? = nil
        var octave: Int? = nil
        var line: Int? = nil
        var int_value: Int? = nil
        var accidental: Accidental? = nil
        var stroke: Int? = nil
        var displaced: Bool? = nil
        
        public init(code : String, shift_right : CGFloat) {
            self.code = code
            self.shift_right = shift_right
        }
    }
    
    public static let KeyValues : [String : KeyProperties] = [
        /* Diamond */
        "D0":  KeyProperties(code: "v27", shift_right: -0.5),
        "D1":  KeyProperties(code: "v2d", shift_right: -0.5),
        "D2":  KeyProperties(code: "v22", shift_right: -0.5),
        "D3":  KeyProperties(code: "v70", shift_right: -0.5),
        
        /* Triangle */
        "T0":  KeyProperties(code: "v49", shift_right: -2),
        "T1":  KeyProperties(code: "v93", shift_right: 0.5),
        "T2":  KeyProperties(code: "v40", shift_right: 0.5),
        "T3":  KeyProperties(code: "v7d", shift_right: 0.5),
        
        /* Cross */
        "X0":  KeyProperties(code: "v92", shift_right: -2),
        "X1":  KeyProperties(code: "v95", shift_right: -0.5),
        "X2":  KeyProperties(code: "v7f", shift_right: 0.5),
        "X3":  KeyProperties(code: "v3b", shift_right: -2)
    ]
    
    public static let IntegerToNote : [Int : String] = [
        0: "C",
        1: "C#",
        2: "D",
        3: "D#",
        4: "E",
        5: "F",
        6: "F#",
        7: "G",
        8: "G#",
        9: "A",
        10: "A#",
        11: "B"
    ]
    
    public static let DurationToTicks : [String : Int] = [
        "1/2":  Vex.RESOLUTION * 2,
        "1":    Vex.RESOLUTION / 1,
        "2":    Vex.RESOLUTION / 2,
        "4":    Vex.RESOLUTION / 4,
        "8":    Vex.RESOLUTION / 8,
        "16":   Vex.RESOLUTION / 16,
        "32":   Vex.RESOLUTION / 32,
        "64":   Vex.RESOLUTION / 64,
        "128":  Vex.RESOLUTION / 128,
        "256":  Vex.RESOLUTION / 256
    ]
    
    public static let DurationAliases : [String:String] = [
        "w": "1",
        "h": "2",
        "q": "4",
        
        // This is the default duration used to render bars (BarNote). Bars no longer
        // consume ticks, so this should be a no-op.
        //
        // TODO(0xfe): This needs to be cleaned up.
        "b": "256"
    ]
    
    static func rangeFromNSRange(nsRange: NSRange, forString str: String) -> Range<String.Index>? {
        let fromUTF16 = str.utf16.startIndex.advancedBy(nsRange.location, limit: str.utf16.endIndex)
        let toUTF16 = fromUTF16.advancedBy(nsRange.length, limit: str.utf16.endIndex)
        
        
        if let from = String.Index(fromUTF16, within: str),
            let to = String.Index(toUTF16, within: str) {
                return from ..< to
        }
        
        return nil
    }
    
    public static func parseNoteDurationString(durationString : String) -> [String : AnyObject]? {
        let regexp = try! NSRegularExpression(pattern: "(\\d*\\/?\\d+|[a-z])(d*)([nrhms]|$)", options: [])
        
        guard let result = regexp.matchesInString(durationString, options: [], range: NSMakeRange(0, durationString.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))).first else { return nil }
        
        if (result.numberOfRanges < 4) {
            return nil
        }
        
        guard let durationRange = rangeFromNSRange(result.rangeAtIndex(1), forString: durationString) else { return nil }
        let duration = durationString.substringWithRange(durationRange)
        
        guard let dotsRange = rangeFromNSRange(result.rangeAtIndex(2), forString: durationString) else { return nil }
        let dots = durationString.substringWithRange(dotsRange).lengthOfBytesUsingEncoding(NSUTF8StringEncoding)
        
        guard let typeRange = rangeFromNSRange(result.rangeAtIndex(3), forString: durationString) else { return nil }
        var type = durationString.substringWithRange(typeRange)
        
        if type.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 {
            type = "n"
        }
        
        return ["duration" : duration, "dots" : dots, "type" : type]
    }
    
    // Used to convert duration aliases to the number based duration.
    // If the input isn't an alias, simply return the input.
    //
    // example: 'q' -> '4', '8' -> '8'
    public static func sanitizeDuration(var duration : String) -> String? {
        if let alias = DurationAliases[duration] {
            duration = alias
        }
        
        if DurationToTicks[duration] == nil {
            return nil
        }
        
        return duration
    }
    
    // Convert the `duration` to total ticks
    public static func durationToTicks(duration : String) -> Int? {
        guard let saneDuration = Vex.sanitizeDuration(duration) else { return nil }
        guard let ticks = Vex.DurationToTicks[saneDuration] else { return nil }
        return ticks
    }
    
    public static func parseNoteData(duration : String, dots : Int? = nil, type : String? = nil) -> [String : AnyObject]? {
        // Preserve backwards-compatibility
        guard let durationStringData = parseNoteDurationString(duration) else { return nil }
        guard let parsedDuration = durationStringData["duration"] as? String else { return nil }
        guard var ticks = durationToTicks(parsedDuration) else { return nil }
        
        var type : String? = type
        if type == nil {
            type = durationStringData["type"] as? String
            if type == nil {
                type = "n"
            }
        }
        else {
            if !(type == "n" || type == "r" || type == "h" || type == "m" || type == "s") {
                return nil
            }
        }
        
        var dots : Int? = dots
        if dots == nil {
            dots = durationStringData["dots"] as? Int
        }
        
        if dots == nil {
            return nil
        }
        
        var currentTicks = ticks
        for (var i = 0; i < dots; i++) {
            if (currentTicks <= 1) {
                return nil
            }
            
            currentTicks = currentTicks / 2
            ticks += currentTicks
        }
        
        return [
            "duration": parsedDuration,
            "type": type!,
            "dots": dots!,
            "ticks": ticks
        ]
    }
    
    public static func durationToGlyph(duration : String, type : String = "n") -> DurationCode.Data? {
        guard let saneDuration = Vex.sanitizeDuration(duration) else { return nil }
        guard let code = Vex.duration_codes[saneDuration] as? [String : AnyObject] else { return nil }
        guard let typeInfo = code["type"] as? [String : AnyObject] else { return nil }
        guard let glyphTypeProperties = typeInfo[type] as? [String : AnyObject] else { return nil }
        guard let common = code["common"] as? [String : AnyObject] else { return nil }
        
        var result = common
        result.unionInPlace(glyphTypeProperties)
        
        return DurationCode.Data(dictionary: result)
    }
    
    public struct DurationCode {
        public class Data : VexDurationCodeProtocol {
            private var dictionary : [String : AnyObject]
            
            var code_head: String? { get { return dictionary["code_head"] as? String }}
            var head_width: CGFloat? { get { return dictionary["head_width"] as? CGFloat }}
            var rest: Bool? { get { return dictionary["rest"] as? Bool }}
            var position: String? { get { return dictionary["position"] as? String }}
            var stem: Bool? { get { return dictionary["stem"] as? Bool }}
            var stem_offset: CGFloat? { get { return dictionary["stem_offset"] as? CGFloat }}
            var flag: Bool? { get { return dictionary["flag"] as? Bool }}
            var stem_up_extension: CGFloat? { get { return dictionary["stem_up_extension"] as? CGFloat }}
            var stem_down_extension: CGFloat? { get { return dictionary["stem_down_extension"] as? CGFloat }}
            var gracenote_stem_up_extension: CGFloat? { get { return dictionary["gracenote_stem_up_extension"] as? CGFloat }}
            var gracenote_stem_down_extension: CGFloat? { get { return dictionary["gracenote_stem_down_extension"] as? CGFloat }}
            var tabnote_stem_up_extension: CGFloat? { get { return dictionary["tabnote_stem_up_extension"] as? CGFloat }}
            var tabnote_stem_down_extension: CGFloat? { get { return dictionary["tabnote_stem_down_extension"] as? CGFloat }}
            var dot_shiftY: CGFloat? { get { return dictionary["dot_shiftY"] as? CGFloat }}
            var line_above: CGFloat? { get { return dictionary["line_above"] as? CGFloat }}
            var line_below: CGFloat? { get { return dictionary["line_below"] as? CGFloat }}
            var beam_count: Int? { get { return dictionary["beam_count"] as? Int }}
            
            public init(dictionary : [String : AnyObject]) {
                self.dictionary = dictionary
            }
        }
        
        var common : [String:AnyObject]
        
        var type : [String : [String:AnyObject]]
    }
    
    public static var duration_codes : [String : AnyObject] {
        get {
            let fontPath = NSBundle.mainBundle().pathForResource("DurationCodes", ofType: "json")!
            var fontString = try! String(contentsOfFile: fontPath)
            fontString = fontString.stringByReplacingOccurrencesOfString("\"NEGATIVE_STEM_HEIGHT\"", withString: "-\(Int(Vex.STEM_HEIGHT))")
            let fontData = fontString.dataUsingEncoding(NSUTF8StringEncoding)!
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(fontData, options: []) as! [String : AnyObject]
                return json
            }
            catch {
                fatalError("Failed to load clef annotation info.")
            }
        }
    }
}
