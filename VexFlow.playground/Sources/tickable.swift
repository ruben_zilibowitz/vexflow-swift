//
//  tickable.swift
//
//
//  tickable.swift
//
//
//  Created by Ruben Zilibowitz on 9/01/2016.
//
//

import UIKit

extension Vex {
    public class Tickable {
        public struct Metrics {
            var width : CGFloat = 0         // `width`: The total width of the note (including modifiers.)
            var noteWidth : CGFloat = 0     // `noteWidth`: The width of the note head only.
            var left_shift : CGFloat = 0    // `left_shift`: The horizontal displacement of the note.
            var modLeftPx : CGFloat = 0     // `modLeftPx`: Start `X` for left modifiers.
            var modRightPx : CGFloat = 0    // `modRightPx`: Start `X` for right modifiers.
            var extraLeftPx : CGFloat = 0   // `extraLeftPx`: Extra space on left of note.
            var extraRightPx : CGFloat = 0  // `extraRightPx`: Extra space on right of note.
        }
        
        var intrinsicTicks : Rational = 0 {
            didSet {
                self.ticks = self.tickMultiplier * self.intrinsicTicks
            }
        }
        var tickMultiplier : Rational = 1
        var ticks : Rational = 0
        var x_shift : CGFloat = 0           // Shift from tick context
        
        // Every tickable must be associated with a voice. This allows formatters
        // and preFormatter to associate them with the right modifierContexts.
        var voice : Voice?
        var tickContext : TickContext?
        var modifierContext : ModifierContext?
        var modifiers : [VexCategory] = []
        var preFormatted : Bool = false
        var postFormatted : Bool = false
        var tuplet : Int? {
            didSet {
                
            }
        }
        
        // For interactivity
        var id : Int?
        var elem : Int?
        
        var align_center : Bool = false
        var center_x_shift : CGFloat = 0 // Shift from tick context if center aligned
        
        // This flag tells the formatter to ignore this tickable during
        // formatting and justification. It is set by tickables such as BarNote.
        var ignore_ticks : Bool = false
        var context : CGContextRef?
        
        var metrics : Metrics? { get { return nil } }
        
        var stave : Stave?
        var boundingBox : CGRect?
        
        public func preFormat() {
        }
        
        public func draw() {
            fatalError("draw() not implemented for this tickable.")
        }
    }
}
