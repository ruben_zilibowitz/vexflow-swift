//
//  formatter.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

import UIKit

extension Vex {
    public class Formatter {
        // Minimum width required to render all the notes in the voices.
        var minTotalWidth : CGFloat = 0
        
        // This is set to `true` after `minTotalWidth` is calculated.
        var hasMinTotalWidth : Bool = false
        
        // The suggested amount of space for each tick.
        var pixelsPerTick : CGFloat = 0
        
        // Total number of ticks in the voice.
        var totalTicks : Rational = Rational(0, 1)!
        
        // Arrays of tick and modifier contexts.
        var tContexts : [TickContext]?
        var mContexts : [ModifierContext]?
        
        public func AlignRestsToNotes(notes : [StemmableNote], align_all_notes : Bool, align_tuplets : Bool) {
        }
        
        // ## Private Helpers
        //
        // Helper function to locate the next non-rest note(s).
        func lookAhead(notes : [Note], rest_line : CGFloat, var i : Int, compare : Bool) -> CGFloat {
            // If no valid next note group, next_rest_line is same as current.
            var next_rest_line : CGFloat = rest_line
            
            // Get the rest line for next valid non-rest note group.
            i++
            while (i < notes.count) {
                if (!notes[i].isrest && !notes[i].ignore_ticks) {
                    next_rest_line = CGFloat(notes[i].getLineForRest())
                    break
                }
                i++
            }
            
            // Locate the mid point between two lines.
            if (compare && CGFloat(rest_line) != next_rest_line) {
                let top = max(CGFloat(rest_line), next_rest_line)
                let bot = min(CGFloat(rest_line), next_rest_line)
                next_rest_line = Vex.midLine(CGFloat(top), b: CGFloat(bot))
            }
            return next_rest_line
        }
        
        // Take an array of `voices` and place aligned tickables in the same context. Returns
        // a mapping from `tick` to `context_type`, a list of `tick`s, and the resolution
        // multiplier.
        //
        // Params:
        // * `voices`: Array of `Voice` instances.
        // * `context_type`: A context class (e.g., `ModifierContext`, `TickContext`)
        // * `add_fn`: Function to add tickable to context.
//        public func createContexts(voices, context_type, add_fn) {
//        }
        
        public init() {
        }
    }
}
