//
//  tickcontext.swift
//
//
//  Created by Ruben Zilibowitz on 9/01/2016.
//
//

import UIKit

extension Vex {
    public class TickContext {
        var currentTick = Rational(0, 1)! {
            didSet {
                self.preFormatted = false
            }
        }
        var maxTicks = Rational(0, 1)!
        var minTicks : Rational?
        var width : CGFloat = 0
        var padding : CGFloat = 3     // padding on each side (width += padding * 2)
        var pixelsUsed : CGFloat = 0
        var x : CGFloat = 0
        var tickables : [Tickable] = []   // Notes, tabs, chords, lyrics.
        var notePx : CGFloat = 0       // width of widest note in this context
        var extraLeftPx : CGFloat = 0  // Extra left pixels for modifers & displace notes
        var extraRightPx : CGFloat = 0 // Extra right pixels for modifers & displace notes
        var align_center : Bool = false
        
        var tContexts : [TickContext] = []   // Parent array of tick contexts
        
        // Ignore this tick context for formatting and justification
        var ignore_ticks : Bool = true
        var preFormatted : Bool = false
        var postFormatted : Bool = false
        var context : CGContextRef?         // Rendering context
        
        public func getCenterAlignedTickables() -> [Tickable] {
            return self.tickables.filter({ tick in return tick.align_center })
        }
        
        public struct ExtraPx {
            var left : CGFloat
            var right : CGFloat
            var extraLeft : CGFloat
            var extraRight : CGFloat
        }
        
        var extraPx : ExtraPx {
            get {
                var left_shift : CGFloat = 0
                var right_shift : CGFloat = 0
                var extraLeftPx : CGFloat = 0
                var extraRightPx : CGFloat = 0
                for tick in self.tickables {
                    guard let metrics = tick.metrics else { continue }
                    extraLeftPx = max(metrics.extraLeftPx, extraLeftPx)
                    extraRightPx = max(metrics.extraRightPx, extraRightPx)
                    if let mContext = tick.modifierContext {
                        left_shift = max(left_shift, mContext.state.left_shift)
                        right_shift = max(right_shift, mContext.state.right_shift)
                    }
                }
                return ExtraPx(left: left_shift, right: right_shift, extraLeft: extraLeftPx, extraRight: extraRightPx)
            }
        }
        
        public func addTickable(tickable : Tickable) {
            if (!tickable.ignore_ticks) {
                self.ignore_ticks = false
                
                let ticks = tickable.ticks
                
                if (ticks > self.maxTicks) {
                    self.maxTicks = ticks
                }
                
                if (self.minTicks == nil) {
                    self.minTicks = ticks
                } else if (ticks < self.minTicks) {
                    self.minTicks = ticks
                }
            }
            
            tickable.tickContext = self
            self.tickables.append(tickable)
            self.preFormatted = false
        }
        
        public func preFormat() {
            if (!self.preFormatted) {
                for tickable in self.tickables {
                    tickable.preFormat()
                    guard let metrics = tickable.metrics else { continue }
                    
                    // Maintain max extra pixels from all tickables in the context
                    self.extraLeftPx = max(self.extraLeftPx, metrics.extraLeftPx + metrics.modLeftPx)
                    self.extraRightPx = max(self.extraRightPx, metrics.extraRightPx + metrics.modRightPx)
                    
                    // Maintain the widest note for all tickables in the context
                    self.notePx = max(self.notePx, metrics.noteWidth)
                    
                    // Recalculate the tick context total width
                    self.width = self.notePx + self.extraLeftPx + self.extraRightPx
                }
            }
        }
        
        public func postFormat() {
            if (!self.postFormatted) {
                self.postFormatted = true
            }
        }
        
        public func getNextContext(tickContext : TickContext) -> TickContext? {
            guard let index = tContexts.indexOf({tc in return tc === tickContext}) else { return nil }
            return (index+1 < tContexts.count ? tContexts[index+1] : nil)
        }
    }
}
