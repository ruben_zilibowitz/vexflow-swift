//
//  clef.swift
//  VexFlowTest
//
//  Created by Ruben Zilibowitz on 12/01/2016.
//  Copyright © 2016 Zilibowitz Productions. All rights reserved.
//

import UIKit

extension Vex {
    static var AnnotationInfo : [String : AnyObject] {
        get {
            let fontPath = NSBundle.mainBundle().pathForResource("ClefAnnotationInfo", ofType: "json")!
            let fontData = NSFileManager.defaultManager().contentsAtPath(fontPath)!
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(fontData, options: []) as! [String : AnyObject]
                return json
            }
            catch {
                fatalError("Failed to load clef annotation info.")
            }
        }
    }
    
    public class Clef : StaveModifier {
        public enum Size {
            case Default
            case Small
            
            func toPoint() -> CGFloat {
                switch self {
                case Default:
                    return 40
                case Small:
                    return 32
                }
            }
            
            func toString() -> String {
                switch self {
                case Default:
                    return "default"
                case Small:
                    return "small"
                }
            }
        }
        
        public struct GlyphInfo {
            var code : String
            var line : CGFloat
        }
        
        public struct Annotation {
            var code: String
            var point: CGFloat
            var line: CGFloat
            var x_shift: CGFloat
            
            public init(annotation : String, clef : ClefType, size : Size) {
                let clefString = clef.toString()
                let sizeString = size.toString()
                let anno_dict = AnnotationInfo[annotation] as! [String : AnyObject]
                self.code = anno_dict["code"] as! String
                
                let sizeInfo = (anno_dict["sizes"] as! [String:AnyObject])[sizeString] as! [String:AnyObject]
                self.point = sizeInfo["point"] as! CGFloat
                
                let attachments = sizeInfo["attachments"] as! [String:[String:AnyObject]]
                self.line = attachments[clefString]!["line"] as! CGFloat
                self.x_shift = attachments[clefString]!["x_shift"] as! CGFloat
            }
        }
        
        var clef : ClefType
        var glyphCode : String { get { return clef.toGlyphInfo().code } }
        var line : CGFloat { get { return clef.toGlyphInfo().line } }
        var size : Size
        var point : CGFloat { get { return self.size.toPoint() } }
        var annotation : Annotation?
        
        public init(clef : ClefType = .Treble, size : Size = .Default, annotation : String?, x : CGFloat = 0) {
            
            self.clef = clef
            self.size = size
            
            if let anno = annotation {
                self.annotation = Annotation(annotation: anno, clef: clef, size: size)
            }
            
            super.init(x: x)
        }
        
        // Add this clef to the start of the given `stave`.
        override func addModifier(stave : Vex.Stave) {
            let glyph = Glyph(code: self.glyphCode, point: self.point, stave: stave)
            self.placeGlyphOnLine(glyph, stave: stave, line: self.line)
            if let anno = self.annotation {
                let attachment = Glyph(code: anno.code, point: anno.point, stave: stave)
                attachment.width = nil
                attachment.x_shift = anno.x_shift
                self.placeGlyphOnLine(attachment, stave: stave, line: anno.line)
                stave.addGlyph(attachment)
            }
            stave.addGlyph(glyph)
        }
        
        // Add this clef to the end of the given `stave`.
        override func addEndModifier(stave : Vex.Stave) {
            let glyph = Glyph(code: self.glyphCode, point: self.point, stave: stave)
            self.placeGlyphOnLine(glyph, stave: stave, line: self.line)
            stave.addEndGlyph(glyph)
            if let anno = self.annotation {
                let attachment = Glyph(code: anno.code, point: anno.point, stave: stave)
                attachment.width = nil
                attachment.x_shift = anno.x_shift
                self.placeGlyphOnLine(attachment, stave: stave, line: anno.line)
                stave.addEndGlyph(attachment)
            }
        }
    }
}
