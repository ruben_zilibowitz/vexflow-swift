//
//  stavemodifier.swift
//
//
//  Created by Ruben Zilibowitz on 3/01/2016.
//
//

import UIKit

extension Vex {
    public class StaveModifier : VexCategory {
        var padding : CGFloat
        var x : CGFloat
        
        init(x : CGFloat = 0) {
            self.x = x
            self.padding = 10
        }
        
        func placeGlyphOnLine(glyph : Vex.Glyph, stave : Vex.Stave, line : CGFloat) {
            glyph.y_shift = stave.getYForLine(line) - stave.getYForGlyphs()
        }
        
        func addToStave(stave : Vex.Stave, firstGlyph : Bool) {
            if !firstGlyph {
                stave.addGlyph(stave.makeSpacer(self.padding))
            }
            self.addModifier(stave)
        }
        
        func addToStaveEnd(stave : Vex.Stave, firstGlyph : Bool) {
            if !firstGlyph {
                stave.addEndGlyph(stave.makeSpacer(self.padding))
            }
            else {
                stave.addEndGlyph(stave.makeSpacer(2))
            }
            self.addEndModifier(stave)
        }
        
        func draw(stave : Vex.Stave, x_shift : CGFloat) {
        }
        
        func addModifier(stave : Vex.Stave) {
            fatalError("addModifier() not implemented for this stave modifier.")
        }
        
        func addEndModifier(stave : Vex.Stave) {
            fatalError("addEndModifier() not implemented for this stave modifier.")
        }
        
        static func format(blahs : [VexCategory], state : Vex.ModifierContext.State, context : Vex.ModifierContext) {
            fatalError("format() not implemented for this stave modifier.")
        }
        
        static func postFormat(blahs : [VexCategory], context : Vex.ModifierContext) {
            fatalError("postFormat() not implemented for this stave modifier.")
        }
    }
}
