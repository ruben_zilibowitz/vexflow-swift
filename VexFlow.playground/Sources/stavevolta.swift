//
//  staverepetition.swift
//
//
//  Created by Ruben Zilibowitz on 7/01/2016.
//
//

import UIKit

extension Vex {
    public class Volta : StaveModifier {
        public enum VoltaType {
            case None
            case Begin
            case Mid
            case End
            case Begin_End
        }
        
        var number : Int
        var y_shift : CGFloat
        var volta : VoltaType
        var font : UIFont
        
        init(volta : VoltaType, number : Int, x : CGFloat, y_shift : CGFloat) {
            self.number = number
            self.y_shift = y_shift
            self.volta = volta
            self.font = UIFont(name: "Helvetica Bold", size: 9)!
            super.init(x: x)
        }
        
        override func draw(stave: Vex.Stave, x_shift: CGFloat) {
            guard let context = stave.context else { return }
            
            var width = stave.width;
            let top_y = stave.getYForTopText(CGFloat(stave.num_lines)) + self.y_shift;
            let vert_height = 1.5 * stave.spacing_between_lines_px;
            switch(self.volta) {
            case .Begin:
                CGContextFillRect(context, CGRectMake(self.x + x_shift, top_y, 1, vert_height))
                break;
            case .End:
                width -= 5;
                CGContextFillRect(context, CGRectMake(self.x + x + width, top_y, 1, vert_height))
                break;
            case .Begin_End:
                width -= 3;
                CGContextFillRect(context, CGRectMake(self.x + x_shift, top_y, 1, vert_height))
                CGContextFillRect(context, CGRectMake(self.x + x_shift + width, top_y, 1, vert_height))
                break;
            case .Mid:
                break
            case .None:
                break
            }
            
            // If the beginning of a volta, draw measure number
            if (self.volta == .Begin || self.volta == .Begin_End) {
                    guard let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as? NSMutableParagraphStyle else { return }
                    textStyle.alignment = NSTextAlignment.Center
                    let textFontAttributes : [String : AnyObject] = [
                        NSFontAttributeName: self.font,
                        NSForegroundColorAttributeName: UIColor.blackColor(),
                        NSParagraphStyleAttributeName: textStyle
                    ]
                    let text = "\(self.number)"
                    let textSize = text.sizeWithAttributes(textFontAttributes)
                    let textRect: CGRect = CGRectMake(self.x + x + 5, top_y + 15, textSize.width, textSize.height)
                    text.drawInRect(textRect, withAttributes: textFontAttributes)
            }
            
            CGContextFillRect(context, CGRectMake(self.x + x, top_y, width, 1))
        }
    }
}
