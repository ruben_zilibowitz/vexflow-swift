//
//  stem.swift
//  VexFlowTest
//
//  Created by Ruben Zilibowitz on 19/01/2016.
//  Copyright © 2016 Zilibowitz Productions. All rights reserved.
//

// ## Description
//
// This file implements the `Stem` object. Generally this object is handled
// by its parent `StemmableNote`.
//

import UIKit

extension Vex {
    public class Stem {
        
        // Stem directions
        enum Direction : CGFloat {
            case Up = 1
            case Down = -1
        }
        
        // Theme
        var width : CGFloat = Vex.STEM_WIDTH
        var height : CGFloat = Vex.STEM_HEIGHT
        
        // Default notehead x bounds
        var x_begin : CGFloat = 0
        var x_end : CGFloat = 0
        
        // Y bounds for top/bottom most notehead
        var y_top : CGFloat = 0
        var y_bottom : CGFloat = 0
        
        // Stem base extension
        var y_extend : CGFloat = 0
        // Stem top extension
        var stem_extension : CGFloat = 0
        
        // Direction of the stem
        var stem_direction : Direction = .Up
        
        // Flag to override all draw calls
        var hide : Bool = false
        
        init(x_begin:CGFloat?=nil,x_end:CGFloat?=nil,y_top:CGFloat?=nil,y_bottom:CGFloat?=nil,y_extend:CGFloat?=nil,stem_extension:CGFloat?=nil,stem_direction:Direction?=nil) {
            
            if let x = x_begin {
                self.x_begin = x
            }
            if let x = x_end {
                self.x_end = x
            }
            if let x = y_top {
                self.y_top = x
            }
            if let x = y_bottom {
                self.y_bottom = x
            }
            if let x = y_extend {
                self.y_extend = x
            }
            if let x = stem_extension {
                self.stem_extension = x
            }
            if let x = stem_direction {
                self.stem_direction = x
            }
        }
        
        // Set the x bounds for the default notehead
        func setNoteHeadXBounds(x_begin : CGFloat, x_end : CGFloat) {
            self.x_begin = x_begin
            self.x_end = x_end
        }
        
        // The the y bounds for the top and bottom noteheads
        func setYBounds(y_top : CGFloat, y_bottom : CGFloat) {
            self.y_top = y_top
            self.y_bottom = y_bottom
        }
        
        // Gets the entire height for the stem
        func getHeight() -> CGFloat {
            return ((self.y_bottom - self.y_top) * self.stem_direction.rawValue) +
                ((self.height + self.stem_extension) * self.stem_direction.rawValue);
        }
        
        // Get the y coordinates for the very base of the stem to the top of
        // the extension
        func getExtents() -> [String : CGFloat] {
            var ys = [self.y_top, self.y_bottom]
            
            var top_pixel = self.y_top
            var base_pixel = self.y_bottom
            let stem_height = self.height + self.stem_extension
            
            for (var i = 0; i < ys.count; ++i) {
                let stem_top = ys[i] + (stem_height * -self.stem_direction.rawValue)
                
                if (self.stem_direction == .Down) {
                    top_pixel = max(top_pixel, stem_top)
                    base_pixel = min(base_pixel, ys[i])
                } else {
                    top_pixel = min(top_pixel, stem_top)
                    base_pixel = max(base_pixel, ys[i])
                }
            }
            
            return [ "topY": top_pixel, "baseY": base_pixel ]
        }
        
        // Apply current style to Canvas `context`
        func applyStyle(context : CGContextRef) {
            // empty
        }
        
        var context : CGContextRef?
        
        // Render the stem onto the canvas
        func draw() {
            guard let ctx = self.context else { fatalError("Can't draw without a canvas context.") }
            
            if (self.hide) {
                return
            }
            
            var stem_x : CGFloat
            var stem_y : CGFloat
            let stem_direction = self.stem_direction
            
            if (stem_direction == .Down) {
                // Down stems are rendered to the left of the head.
                stem_x = self.x_begin + (self.width / 2)
                stem_y = self.y_top + 2
            } else {
                // Up stems are rendered to the right of the head.
                stem_x = self.x_end + (self.width / 2)
                stem_y = self.y_bottom - 2
            }
            
            stem_y += self.y_extend * stem_direction.rawValue
            
            print("Rendering stem - ", "Top Y: ", self.y_top, "Bottom Y: ", self.y_bottom)
            
            // Draw the stem
            CGContextSaveGState(ctx)
            self.applyStyle(ctx)
            CGContextBeginPath(ctx)
            CGContextSetLineWidth(ctx, self.width)
            CGContextMoveToPoint(ctx, stem_x, stem_y)
            CGContextAddLineToPoint(ctx, stem_x, stem_y - self.height)
            CGContextStrokePath(ctx)
            CGContextRestoreGState(ctx)
        }
    }
}
