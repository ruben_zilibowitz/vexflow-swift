//
//  ViewController.swift
//  VexFlowTest
//
//  Created by Ruben Zilibowitz on 12/01/2016.
//  Copyright © 2016 Zilibowitz Productions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen().scale)
        let stave = Vex.Stave(x: 10, y: 0, width: 600)
        stave.addClef(.Treble, size: .Default, annotation: "8va")
        stave.addClef(.Treble, size: .Small, annotation: "8vb")
        stave.addClef(.Bass, size: .Default, annotation: "8vb")
        
        stave.context = UIGraphicsGetCurrentContext()
        stave.draw()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imageView = UIImageView(image: image)
        self.view.addSubview(imageView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

