//
//  voicegroup.swift
//
//
//  Created by Ruben Zilibowitz on 10/01/2016.
//
//

import UIKit

extension Vex {
    // Every tickable must be associated with a voiceGroup. This allows formatters
    // and preformatters to associate them with the right modifierContexts.
    public class VoiceGroup {
        var voices : [Voice] = []
        var modifierContexts : [ModifierContext] = []
        
        public func addVoice(voice : Voice) {
            self.voices.append(voice)
            voice.voiceGroup = self
        }
    }
}
