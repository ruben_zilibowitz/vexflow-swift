//
//  glyph.swift
//
//
//  Created by Ruben Zilibowitz on 3/01/2016.
//
//

import UIKit

extension Vex {
    public class GlyphSpacer {
        var context : CGContextRef?
        var width : CGFloat?
        var stave : Vex.Stave
        init(stave : Vex.Stave, width : CGFloat? = nil) {
            self.stave = stave
            self.width = width
        }
        public func renderToStave(x : CGFloat) {
        }
    }
    public class Glyph : GlyphSpacer {
        var code : String
        var glyph : Vex.Font.Glyph {
            get {
                guard let glyph = font.glyphs[code] else { fatalError("Code \(code) does not exist for font.") }
                return glyph
            }
        }
        var point : CGFloat
        var font : Vex.Font
        var x_shift : CGFloat
        var y_shift : CGFloat
        var scale : CGFloat
        
        init(code:String, point:CGFloat, stave:Vex.Stave, font:Font = Vex.defaultFont, x_shift:CGFloat = 0, y_shift:CGFloat = 0, scale:CGFloat = 1) {
            self.font = font
            self.code = code
            self.point = point
            self.x_shift = x_shift
            self.y_shift = y_shift
            self.scale = scale
            super.init(stave: stave)
            reset()
            self.width = (self.glyph.x_max - self.glyph.x_min) * self.scale
        }
        
        public func reset() {
            self.scale = self.point * 72 / (self.font.resolution * 100)
        }
        
        public func render(ctx : CGContextRef, x_pos : CGFloat, y_pos : CGFloat) {
            glyph.renderOutline(ctx, scale: self.scale, x_pos: x_pos, y_pos: y_pos)
        }
        
        public override func renderToStave(x : CGFloat) {
            guard let context = self.context else { return }
            
            glyph.renderOutline(context, scale: self.scale, x_pos: x + self.x_shift, y_pos: stave.getYForGlyphs() + self.y_shift)
        }
    }
}
